#include <iostream>
#include<limits>
#include<conio.h>
#include<math.h>
using namespace std;

void func1();
void func2();
void func3();
void func4();
void func5();
void func6();
void func7();
void func8();

void func1()
{
	float fah, cel;

	cout << "Convert Celsius to Fahrenheit" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter value in celcius : ";
	cin >> cel;
	fah = (cel*1.8) + 32;
	cout << "The calculated fahrenheit is : " << fah << endl;
	cout << "--------------------------------------" << endl;
}

void func2()
{
	float fah, cel;
	cout << "Convert Fahrenheit to Celsius" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter value in fahrenheit : ";
	cin >> fah;
	cel = (fah - 32) / 1.8;
	cout << "The calculated  is celcius :  " << cel << endl;
	cout << "--------------------------------------" << endl;
}

void func3()
{
    const double p1 = 3.14;
	double radius;
	double diameter;
	double circumference;
	cout << "Calculate Circumference of a circle" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the radius of circle: ";
	cin >> radius;
	while ((1 > radius) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input! please try again." << endl;
		cin >> radius;
	}

	diameter = 2 * radius;
	circumference = p1 * diameter;
	cout << "C=The circumference of a circle is : " << circumference << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}

void func4()
{
    const double p1 = 3.14;
	double area;
	double radius;
	cout << "Calculate Area of a circle" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the radius of circle: ";
	cin >> radius;
	while ((1 > radius) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> radius;
	}

	area = p1 * (radius*radius);
	cout << "The area of circle is: " << area << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}
void func5()
{
	int area, l, h;
	cout << "Area of Rectangle" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the length of Rectangle: ";
	cin >> l;
	while ((1 > l) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> l;
	}
	cout << "Please enter the height of Rectangle: ";
	cin >> h;
	while ((1 > h) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> h;
	}
	area = l * h;
	cout << "The area of rectangle is: " << area << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}

void func6()
{
	float a, b, c, area, s;
	cout << "Area of Triangle" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the first value of triangle: ";
	cin >> a;
	while ((1 > a) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> a;
	}
	cout << "Please enter the second value of triangle: ";
	cin >> b;
	while ((1 > b) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> b;
	}
	cout << "Please enter the third value of triangle: ";
	cin >> c;
	while ((1 > c) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> c;
	}
	s = (a + b + c) / 2;
	area = sqrt(s*(s - a)*(s - b)*(s - c));
	cout << "The area of triangle is: " << area << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}

void func7()
{
    const double pi = 3.14;
	double radius;
	double height;
	double volume;
	cout << "Volume of Cylinder" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the radius of the cylinder: " << endl;
	cin >> radius;
	while ((1 > radius) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> radius;
	}
	cout << "Please enter the height of cylinder: " << endl;
	cin >> height;
	while ((1 > height) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> height;
	}
	radius = radius * radius;
	volume = (pi * radius * height);
	cout << "The volume of the cylinder is: " << volume << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}

void func8()
{
    const double pi = 3.14;
	double radius;
	double height;
	double volume;
	cout << "Volume of Cone" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the radius of the cone: " << endl;
	cin >> radius;
	while ((1 > radius) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> radius;
	}
	cout << "Please enter the height of cone: " << endl;
	cin >> height;
	while ((1 > height) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> height;
	}
	radius = radius * radius;
	height = height / 3;
	volume = (pi * radius * height);
	cout << "The volume of the cone is: " << volume << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}



//1 If classes are required for each option in the menu program instead of main ,please modify it
int main()
{
   //Declaration of variables
    float fahrenheit, celcius;
    int choice;

    //3 Option in menus and input

    cout << "1 : Convert Celsius to Fahrenheit " << endl;
    cout << "2 : Convert Fahrenheit to Celsius " << endl;
    cout << "3. Calculate Circumference of a circle" << endl;
    cout << "4. Calculate Area of a circle" << endl;
    cout << "5. Area of Rectangle" << endl;
    cout << "6. Area of Triangle (Heron¡¯s Formula)" << endl;
    cout << "7. Volume of Cylinder" << endl;
    cout << "8. Volume of Cone" << endl;
    cout << "9. Quit program" << endl;
    cin >> choice;
    while (cin.fail() || choice < 1 || choice > 9)
		{
			cin.clear();
			cin.ignore();
			cout << "Invalid Option! Please try again. " << endl;
			cin >> choice;
		}
    // Running option in switch case statement
    switch(choice)
    {
        case 1: {
			cin.clear();
			cin.ignore();
			func1();
			break;
                }
        case 2: {
			cin.clear();
			cin.ignore();
			func2();
			break;
                }

        case 3:
            {
                cin.clear();
                cin.ignore();
                func3();
                break;
			}

		case 4:
		    {
		        cin.clear();
		        cin.ignore();
		        func4();
		        break;
            }
		case 5: {
			cin.clear();
			cin.ignore();
			func5();
			break;
		}
		case 6: {
			cin.clear();
			cin.ignore();
			func6();
			break;
		}
        case 7: {
			cin.clear();
			cin.ignore();
			func7();
			break;
		}
        case 8: {
			cin.clear();
			cin.ignore();
			func8();
			break;
		}

    default:
     cout << "Please enter a valid input" << endl;
    }

    return 0;
}


